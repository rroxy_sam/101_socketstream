var express = require('express');
var app = express();
var fs = require('fs');
var path = require('path');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var ss = require('socket.io-stream');
var mongoose = require('mongoose');
mongoose.connect('mongodb://socketio:socketio@ds021884.mlab.com:21884/socketio');
//mongoose.connect('mongodb://127.0.0.1:27017/myApp');
var conn = mongoose.connection;
var Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;

var chatObj = require('./model.js');

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res){
     res.render('index.html');
});

io.on('connection', function(socket){
     ss(socket).on('sendFile', function(stream, data) {
          var filename = path.basename(data.name);
          var newFilename = new Date().getTime() + "_" + filename;
          var buffer = "";
          var mimeType = data.mimeType;
          var gfs = Grid(conn.db);
          var writestream = gfs.createWriteStream({
               content_type: mimeType,
               filename: newFilename
          });

          stream.pipe(writestream);
          writestream.on('close', function (file) {
               var readStream = gfs.createReadStream({
                    filename: newFilename
               });
               readStream.setEncoding('base64');
               readStream.on("data", function (chunk) {
                    buffer += chunk;
               });
               readStream.on("end", function () {
                    io.emit('receiveFile', { mediaType: (mimeType.split('/')[0]), buffer: buffer, mimeType : mimeType});
               });
          });
     });
});

http.listen(4000, function(){
     console.log('listening on *:4000');
});
