var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var modelSchema = new Schema({
    img: {name : String,  data: Buffer, mimeType: String }
});

var modelObj = mongoose.model('Chat', modelSchema);
module.exports = modelObj;
